import { DataTypes } from 'sequelize';

const init = orm => {
  const Post = orm.define(
    'post',
    {
      body: {
        allowNull: false,
        type: DataTypes.TEXT
      },
      deleted: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return Post;
};

export { init };
