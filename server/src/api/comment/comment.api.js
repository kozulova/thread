import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.$ID, (req, res, next) => commentService
      .updateCommentById(req.params.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .delete(CommentsApiPath.$ID, (req, res, next) =>{ 
      //res.send(req.params.id);
      const id = req.params.id;
      //res.send(JSON.stringify(id));
      commentService
      .deleteById(id)
      .then(comment=>res.send(JSON.stringify(id)))
      .catch(next)
    }
    )
    .put(CommentsApiPath.REACT, (req, res, next) => {
      commentService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        return res.send(reaction);
      })
      .catch(next)
      }
      )

  return router;
};

export { initComment };
