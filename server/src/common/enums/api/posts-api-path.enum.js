const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  DISLIKE: '/dislike'
};

export { PostsApiPath };
