/* eslint-disable no-confusing-arrow */
import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  EDIT_MODE_POST: 'thread/edit-post',
  EDIT_MODE_COMMENT: 'thread/edit-comment'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const setEditModePost = createAction(ActionType.EDIT_MODE_POST, post => ({
  payload: {
    post
  }
}));

const setEditModeComment = createAction(ActionType.EDIT_MODE_COMMENT, comment => ({
  payload: {
    comment
  }
}));

const toogleEditComment = commentId => async (dispatch, getRootState) => {
  console.log(commentId);
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  console.log(comment);
  const state = getRootState();
  console.log(state);
  dispatch(setEditModeComment(comment));
};

const updatePost = post => async (dispatch, getRootState) => {
  const newPost = await postService.updatePost(post.id, { body: post.body, imageId: post.imageId });
  const {
    posts: { posts }
  } = getRootState();
  const updatedPosts = posts.map(p => p.id === newPost.id ? newPost : p);
  dispatch(setPosts(updatedPosts));
};

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const toogleEditModePost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  console.log(post, 'edit');
  dispatch(setEditModePost(post));
};

const cancelEdit = () => async (dispatch, getRootState) => {
  const state = getRootState();
  console.log(state);
  dispatch(setEditModePost(null));
};

const likePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.dislikePost(postId);

  const diff = id ? 1 : -1;

  const mapdisLikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== postId ? post : mapdisLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapdisLikes(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const editComment = (id, comment) => async (dispatch, getRootState) => {
  const updatedComment = await commentService.updateComment(id, comment);
  const {
    posts
  } = getRootState();
  const updatedComments = posts.expandedPost.comments.map(c => c.id === id ? { ...c, ...comment } : c);
  const updatedExpandedPost = { ...posts.expandedPost, comments: updatedComments };
  dispatch(setExpandedPost(updatedExpandedPost));
  dispatch(setEditModeComment(undefined));
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const id = await commentService.deleteComment(commentId);
  const {
    posts
  } = getRootState();
  const updatedComments = posts.expandedPost.comments.filter(c => c.id !== commentId);
  const updatedExpandedPost = { ...posts.expandedPost, comments: updatedComments };
  console.log(updatedExpandedPost);
  dispatch(setExpandedPost(updatedExpandedPost));
  dispatch(setEditModeComment(undefined));
};

const deletePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.deletePost(postId);
  const {
    posts: { posts }
  } = getRootState();
  const currentPosts = posts.filter(post => post.id !== id);
  dispatch(setPosts(currentPosts));
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  setEditModePost,
  setEditModeComment,
  toogleEditComment,
  editComment,
  deleteComment,
  updatePost,
  cancelEdit,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  toogleEditModePost,
  likePost,
  addComment,
  dislikePost,
  deletePost
};
