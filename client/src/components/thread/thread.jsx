/* eslint-disable max-len */
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost, EditPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, editModePost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    editModePost: state.posts.editModePost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [hideOwnPosts, setHideOwnPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDisLike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleEditMode = React.useCallback(id => (
    dispatch(threadActionCreator.toogleEditModePost(id))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };
  const handlePostEdit = React.useCallback(postPayload => (
    dispatch(threadActionCreator.updatePost(postPayload))
  ), [dispatch]);
  const handleCancelEdit = () => {
    dispatch(threadActionCreator.cancelEdit());
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toogleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    postsFilter.userIdHide = hideOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        {editModePost ? <EditPost onPostEdit={handlePostEdit} onCancelEdit={handleCancelEdit} uploadImage={uploadImage} post={editModePost} />
          : <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} /> }
      </div>
      <div className={styles.toolbar}>
        {hideOwnPosts
        || (
          <Checkbox
            toggle
            label="Show only my posts"
            checked={showOwnPosts}
            onChange={toggleShowOwnPosts}
          />
        )}
        {showOwnPosts
        || (
          <Checkbox
            toggle
            label="Hide my post"
            checked={hideOwnPosts}
            onChange={toogleHideOwnPosts}
          />
        )}
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDisLike={handlePostDisLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            onPostEdit={userId === post.user.id ? handleEditMode : null}
            onPostDelete={userId === post.user.id ? handlePostDelete : null}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
