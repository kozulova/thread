/* eslint-disable max-len */
/* eslint-disable indent */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */

import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment } from 'src/components/common/common';

import styles from './styles.module.scss';

const EditPost = ({ onPostEdit, uploadImage, onCancelEdit, post }) => {
  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image ? { imageId: post.image.id, imageLink: post.image.link } : undefined);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await onPostEdit({ id: post.id, imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    onCancelEdit();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Segment>
      <Form onSubmit={handleEditPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink || post.image} alt="post" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} onClick={onCancelEdit}>
            Cancel
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Edit
          </Button>
        </div>
      </Form>
    </Segment>
  );
};

EditPost.propTypes = {
  onPostEdit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onCancelEdit: PropTypes.func.isRequired,
  post: PropTypes.shape.isRequired
};

export default EditPost;
