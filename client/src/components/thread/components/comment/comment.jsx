/* eslint-disable react/jsx-indent */
import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import PropTypes from 'prop-types';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { ButtonType } from 'src/common/enums/enums';
import { Comment as CommentUI, Label, Form, Button } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

const EditForm = ({ id, body, onCommentEditMode, handleCommentEdit }) => {
  const [comment, setComment] = React.useState(body);
  const editComment = async () => {
    if (!body) {
      return;
    }
    await handleCommentEdit(id, { body: comment });
  };

  return (
    <Form onSubmit={console.log('Submit')}>
      <Form.TextArea
        name="body"
        value={comment}
        onChange={ev => setComment(ev.target.value)}
      />
      <Button onClick={() => onCommentEditMode()}>
        Cancel
      </Button>
      <Button type={ButtonType.SUBMIT} onClick={editComment}>
        Edit Comment
      </Button>
    </Form>
  );
};

const Comment = ({ comment: { id, body, createdAt, user },
  onCommentEditMode, editModeComment, handleCommentEdit, onCommentDelete }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
      <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
      {
        (editModeComment && id === editModeComment.id)
          ? (
            <CommentUI.Text>
              <EditForm
                body={editModeComment.body}
                onCommentEditMode={onCommentEditMode}
                id={id}
                handleCommentEdit={handleCommentEdit}
              />
            </CommentUI.Text>
          )
          : <CommentUI.Text>{body}</CommentUI.Text>
      }
      <Label
        basic
        size="small"
        as="a"
        className={styles.toolbarBtn}
        onClick={() => onCommentEditMode(id)}
      >
        Edit
      </Label>
      <Label
        basic
        size="small"
        as="a"
        className={styles.toolbarBtn}
        onClick={() => onCommentDelete(id)}
      >
        Delete
      </Label>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentEditMode: PropTypes.func.isRequired,
  editModeComment: PropTypes.shape.isRequired,
  handleCommentEdit: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

EditForm.propTypes = {
  body: PropTypes.shape.isRequired,
  onCommentEditMode: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  handleCommentEdit: PropTypes.func.isRequired
};

export default Comment;
