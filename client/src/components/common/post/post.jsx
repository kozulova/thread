/* eslint-disable react/require-default-props */
/* eslint-disable max-len */
import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, SemanticUiHeader } from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({ post, onPostLike, onPostDisLike, onExpandedPostToggle, sharePost, onPostEdit, onPostDelete }) => {
  const [showReaction, setShowReaction] = React.useState(false);
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    postReactions
  } = post;
  const date = getFromNowTime(createdAt);
  const handlePostLike = () => onPostLike(id);
  const handlePostDisLike = () => onPostDisLike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostEdit = () => {
    window.scroll(0, 0);
    onPostEdit(id);
  };
  const handlePostDelete = () => onPostDelete(id);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
          onMouseEnter={() => setShowReaction(true)}
          onMouseLeave={() => setShowReaction(false)}
        >
          {(postReactions.length > 0) && showReaction && (<SemanticUiHeader>{postReactions.map(reaction => reaction.user.username.charAt(0).toUpperCase())}</SemanticUiHeader>)}
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDisLike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {onPostEdit && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => handlePostEdit(id)}
          >
            Edit
          </Label>
        )}
        {onPostDelete && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => handlePostDelete(id)}
          >
            Delete
          </Label>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDisLike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func,
  onPostDelete: PropTypes.func
};

export default Post;
