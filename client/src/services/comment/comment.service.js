import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(id, comment) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(comment)
    });
  }

  deleteComment(id) {
    // console.log(id);
    return this._http.load(`api/comments/${id}`, {
      method: HttpMethod.DELETE
    });
  }
}

export { Comment };
